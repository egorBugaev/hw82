const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});


const upload = multer({ storage: multer.memoryStorage({}) });

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Album.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
	    albumData.image = new Buffer(req.file.buffer).toString("base64");
    } else {
      albumData.image = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
	  Album.find().populate('artist')
      .findOne({_id: req.params.id})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;