const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const CategorySchema = new Schema({
	title: {
		type: String,
		required: true,
		unique: true
	},
	artist: {
		type: Schema.Types.ObjectId,
		ref: 'Artist',
		required: true
	},
	tracks:{
		type: Array,
		required: true
	},
	image: String,
	description: String
});

const Category = mongoose.model('Album', CategorySchema);

module.exports = Category;