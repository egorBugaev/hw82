const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const CategorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: String,
    image: String


});

const Artist = mongoose.model('Artist', CategorySchema);

module.exports = Artist;